/*********************************************
 * OPL 12.6.1.0 Model
 * Author: A.Omu
 * Creation Date: Jun 11, 2015 at 1:47:31 PM
 *********************************************/

 /*****************************************************************************
 ******************************************************************************
 INPUT PARAMETERS
 ******************************************************************************
 ******************************************************************************/
 
 //INDEX DECLARATIONS
 {string} Tech = ...; // i 
 {int} Period = asSet(1..8760);  //p. 1 hour periods
 {int} EndUse = asSet(1..2); //u. 1 = electricity; 2 = heating;

 //WEATHER DATA
 float Solar[Period] = ...; //average solar radiation in each period (W/m2)
 
 //TECHNOLOGY DATA
 float MaxNumber[Tech]=...; //Maximum number of each discrete unit
 float MaxCap[Tech]=...; //Maximum capacity (kW)
 float FixedCapCost[Tech]=...; //Fixed Capital Cost (kW)
 float VariableCapCost[Tech]=...; //Variable Capital cost (chf per kW)
 float OMVCost[Tech]=...; //Variable O&M cost (chf per kWh)
 float Efficiency[Tech]=...; //Generation efficiency (%)
 float Lifetime[Tech]=...; //Unit lifetime (years)
 float htp[Tech]=...; //heat to power ratio
 float MinLoad[Tech]=...; //Minimum acceptable load (%)
 float Appl[Tech]=...; //Grouping of technologies 
 float CO2EF[Tech]=...; //technology CO2 emission factor (based on fuel type)

 //Battery Data
 float CostBat = ...; //Battery capital cost 
 float MaxBat = ...; //Maximum size of the battery
 float LifeBat = ...; //Battery lifetime
 float ch_eff = ...; //Battery charging efficiency 
 float disch_eff = ...; //Battery discharging efficiency
 float decay = ...; //Battery hourly decay
 float max_ch = ...; //Battery max charging rate
 float max_disch = ...; //BAttery max discharging rate
 float min_state = ...; //Battery minimum state of charge
 
 //Thermal Energy Storage Tank Data
 float CostTES = ...; //TES capital cost 
 float MaxTES = ...; //Maximum size of the TES
 float LifeTES = ...; //TES lifetime
 float ceff_TES = ...; //TES charging efficiency 
 float deff_TES = ...; //TES discharging efficiency
 float heatloss = ...; //TES heat loss
 float maxch_TES = ...; //TES max charging rate
 float maxdisch_TES = ...; //TES max discharging rate
 
 //SOLAR DATA 
 float MaxRoof = ...; //Maximum available roof area 
 
 //ECONOMIC DATA 
 float eprice = ...;   //electricity price
 float gprice = ...;   //gas price
 float esell = ...; //Feed in tarrif
 float intrate = ...;   //interest rate 
 float Annuity[i in Tech] = intrate/(1 - (1/((1+intrate)^(Lifetime[i])))); //Annuity factor for each technology
 float AnnuityTES = intrate/(1 - (1/((1+intrate)^(LifeTES)))); //Annuity factor for the TES
 float AnnuityBat = intrate/(1 - (1/((1+intrate)^(LifeBat)))); //Annuity factor for battery
 
 
 //EMISSIONS/ENVIRONMENTAL DATA
 float GridCO2EF = ...; //Grid CO2 Emission Factor
 float NGCO2EF = ...; //Natural Gas CO2 Emission Factor
 float CO2Limit =...; //Target emissions reduction
 float hdump = ...; //Heat dump limit
 float RefBoilerEff = ...; //Reference boiler efficiency

 //DEMAND DATA
 float Demand[Period][EndUse] = ...; //demand of electricity and heat in each hour

 //MISC INPUT PARAMETERS
 int Suit[Tech][EndUse] = ...; //Suitability of each technology to generate each end use
 int M1 = ...; //Arbitary large number
 
 /*****************************************************************************
 ******************************************************************************
 MODEL DECLARATIONS
 ******************************************************************************
 ******************************************************************************/
 
 //DECISiON VARIABLES
 dvar boolean PurFC[Tech]; //Binary variable that is used to include a fixed capital cost if a technology has been purchased
 dvar int+ Unit[Tech];  //Number of units of each discrete technology that are purchased
 dvar float+ Cap[Tech]; //Capacity of the two continuous technologies (kW)
 dvar int+ Operate[Period]; //Number of CHP units operating in each time interval
 dvar float+ Gen[Period][Tech][EndUse];  //Energy generation by each technology during each time period
 dvar float+ ElecPur[Period]; //Electricity purchased from grid during each time period 
 dvar float+ ElecSell[Period]; //Excess electricity sold back to the grid during every time period 
 dvar float+ dump[Period][Tech]; //Excess heat that is dumped by the CHP 

 dvar int y1[Period] in 0..1; //binary control variable

 dvar float+ BatCap; //Battery capacity installed
 dvar float+ Charge_Bat[Period]; //Electricity used to charge the battery (kWh)
 dvar float+ Discharge_Bat[Period]; //Electricity discharged from the battery (kWh)
 dvar float+ Stored_Bat[Period]; //Electricity currently stored in the battery (kWh)
 
 dvar float+ TESCap; //TES capacity installed
 dvar float+ Charge_TES[Period]; //Heat used to charge the TES (kWh)
 dvar float+ Discharge_TES[Period]; //Heat discharged from the TES (kWh)
 dvar float+ Stored_TES[Period]; //Heat currently stored in the TES (kWh)
 
 dvar float+ CO2Emissions; //Total CO2 emissions for the optimal energy system over the entire time horizon (kgCO2)
 
 //Objective function equation decomposed by type of cost
 dexpr float FixedCapCostTech = //Annual fixed capital cost of technologies 
    sum(i in Tech) 
    (PurFC[i]*Annuity[i]*FixedCapCost[i]);
    
 dexpr float VariableCapCostTech = //Annual variable capital cost of technologies 
    sum(i in Tech) 
    (Unit[i]*MaxCap[i]*Annuity[i]*VariableCapCost[i] + Cap[i]*Annuity[i]*VariableCapCost[i]);
 
 dexpr float CapCostTES = //Annual capital cost of the TES 
    TESCap*AnnuityTES*CostTES;
    
 dexpr float CapCostBattery = //Annual capital cost of the battery 
    BatCap*AnnuityBat*CostBat;
        
 dexpr float OMVCostTotal = //Variable O&M cost of technologies
   	sum(p in Period, i in Tech, u in EndUse) 
   	OMVCost[i]*Gen[p][i][u];
   
 dexpr float NGCost1 = //Cost of natural gas consumed by CHP
 	sum(p in Period, i in Tech: Appl[i] == 5, u in EndUse: u == 1) 
 	gprice*(Gen[p][i][u]/Efficiency[i]);
 		
 dexpr float NGCost2 = //Cost of natural gas consumed by boiler
 	sum(p in Period, i in Tech: Appl[i] == 3, u in EndUse: u == 2) 
 	gprice*(Gen[p][i][u]/Efficiency[i]);
 	 	
 dexpr float ElecCost = //Cost of electricty purchased
 	sum(p in Period) 
 	eprice*ElecPur[p];
 	
dexpr float ElecSold = //Cost of electricty sold back to grid
 	sum(p in Period) 
 	esell*ElecSell[p];
 	
	
 /******************************************************************************
 ******************************************************************************  
 MODEL
 ******************************************************************************
 ******************************************************************************/


 //Objective Function
 minimize 	FixedCapCostTech + VariableCapCostTech
 			+ CapCostTES
 			+ CapCostBattery 
 			+ OMVCostTotal 
 			+ NGCost1 + NGCost2
 			+ ElecCost - ElecSold;
 
 //Constraints
 subject to {
 
   //OPERATION CONSTRAINTS
   //Number of units operating in each time interval has to be less than or equal to the number of units that were purchased
   forall (p in Period, i in Tech: Appl[i] == 5) {
   Ops: Operate[p] <= Unit[i];  
   }
     
   //DICRETE VERSUS CONTINUOUS TECHNOLOGIES
   //The capacity of the heat pump or boiler has to be less than the maximum allowable capacity 
   forall (i in Tech: Appl[i] < 4) {
   continuous: Unit[i] == 0;
   maxcap: Cap[i] <= MaxCap[i];
   }
   
   
   forall (i in Tech) {
   pur: PurFC[i]*M1 >= Cap[i] + Unit[i];
   }
   
   //The number of CHP, ST, and PVs installed has to be less than the maximum number
   forall (i in Tech: Appl[i] > 4) {
   discrete: Cap[i] == 0;  
   maxunits: Unit[i] <= MaxNumber[i]; 
   }
   
   //ENERGY BALANCE CONSTRAINTS 
   //Electricity Balance
   forall (p in Period, u in EndUse : u == 1) {
   ElecDemand:  sum(i in Tech) Gen[p][i][u] + ElecPur[p] + Discharge_Bat[p]*disch_eff
   				== Demand[p][u] + Charge_Bat[p]/ch_eff + ElecSell[p] +  sum(i in Tech: i == "HP") Gen[p][i][2]/Efficiency[i];  
   } 
   			   
   //Heat Balance
   forall (p in Period, u in EndUse : u == 2) {
   HeatDemand:   sum(i in Tech) Gen[p][i][u] + Discharge_TES[p]*deff_TES
   				== Demand[p][u] + Charge_TES[p]/ceff_TES;  
   }    
     
   //SELLING and BUYING ELECTRICITY 
   //You cannot buy electricity from the grid and sell electricity to the grid at the same time
   forall (p in Period) {
   GridBuy: ElecPur[p]  <= M1*y1[p];
   GridSell: ElecSell[p] <= M1*(1-y1[p]);
   }
       
   //CAPACITY CONSTRAINTS AND TECHNOLOGY MODELS
   //CHP
   forall (p in Period, i in Tech:  Appl[i]  == 5, u in EndUse: u == 1) {
   MinCHP: Gen[p][i][u] >= Operate[p]*MaxCap[i]*Suit[i][u]*MinLoad[i];
   MaxCHP: Gen[p][i][u] <= Operate[p]*MaxCap[i]*Suit[i][u];
   } 
      
   //Heat Recovery from CHP units
   forall (p in Period, i in Tech: Appl[i] == 5) {
   HeatRecovery:  sum (u in EndUse: u == 2) Gen[p][i][u] + dump[p][i] 
   					== Gen[p][i][1]*htp[i];
   }
   
   //Limiting the amount of heat that chps can dump
   forall (p in Period, i in Tech: Appl[i] == 5) {
   HeatDump: dump[p][i] <= sum (u in EndUse: u == 2) Gen[p][i][u]*hdump;
   }
   
   //No other technologies can dump heat
   forall (p in Period, i in Tech: Appl[i] != 5) {
   HeatDump2: dump[p][i] == 0;
   } 
    
   //BOILERS
   forall (p in Period, i in Tech: Appl[i] == 3, u in EndUse) {
   MinB: Gen[p][i][u] >= Cap[i]*Suit[i][u]*MinLoad[i];
   MaxB: Gen[p][i][u] <= Cap[i]*Suit[i][u];
   } 
    
   //PV and Solar Thermal
   forall (p in Period, i in Tech: Appl[i] == 11, u in EndUse) {
   SolarCap: Gen[p][i][u] == Unit[i]*(Solar[p]/1000)*Efficiency[i]*Suit[i][u];
   SolarArea: sum(i in Tech) Unit[i] <= MaxRoof;
   }  
   
   //HP
   forall (p in Period, i in Tech: Appl[i] < 3, u in EndUse) {
   MinHP: Gen[p][i][u] >= Cap[i]*Suit[i][u]*MinLoad[i];
   MaxHP: Gen[p][i][u] <= Cap[i]*Suit[i][u];
   } 
   
   //BATTERY MODEL
   //Initial state of battery is set equal to the minimum state of charge
   forall (p in Period: p == 1) {
   batstate0: Stored_Bat[p] == BatCap*min_state;
   }
   
   //Battery energy balance
   forall (p in Period: p > 1) {
   batstate: Stored_Bat[p] == Stored_Bat[p-1]*(1-decay) + Charge_Bat[p] - Discharge_Bat[p];
   }
   
   //Battery charging and discharging limits
   forall (p in Period) {
   minsoc: Stored_Bat[p] >= BatCap*min_state; 
   batcharging: Charge_Bat[p] <= BatCap*max_ch;
   batdischarging: Discharge_Bat[p] <= BatCap*max_disch;
   maxbatsize: BatCap <= MaxBat;
   }
   
   //Battery sizing
   forall (p in Period: p > 1) {
   batstate2: Stored_Bat[p] <= BatCap;
   }
   
   
   //TES Model
   //Cyclical annual storage
   forall (p in Period: p == 1) {
   tesstate0: Stored_TES[p] == Stored_TES[8760];
   tesdischarging0: Discharge_TES[p] == 0;
   }
   
   //Thermal storage balance
   forall (p in Period: p > 1) {
   tesstate: Stored_TES[p] == Stored_TES[p-1]*(1-heatloss) + Charge_TES[p] - Discharge_TES[p];
   }
   
   //Thermal storage charging and discharging limits
   forall (p in Period) {
   tescharging: Charge_TES[p] <= TESCap*maxch_TES;
   tesdischarging: Discharge_TES[p] <= TESCap*maxdisch_TES;
   }
   
   //Thermal storage sizing
   forall (p in Period) {
   tessizing: Stored_TES[p] <= TESCap; 
   maxtes: TESCap <= MaxTES;  
   }
   
   //EMISSIONS TARGETS
   TotalEms:  CO2Emissions == sum(p in Period) ElecPur[p]*GridCO2EF 
   							+ sum(p in Period, u in EndUse, i in Tech: i != "CHP") CO2EF[i]*Gen[p][i][u]/Efficiency[i]
   							+ sum(p in Period, u in EndUse: u == 1, i in Tech: i == "CHP") CO2EF[i]*Gen[p][i][u]/Efficiency[i];
   
   EmLimit: CO2Emissions <= sum(p in Period, u in EndUse: u == 1) Demand[p][u]*GridCO2EF*(1-CO2Limit)
   							+ sum(p in Period, u in EndUse: u == 2) (NGCO2EF*Demand[p][u]/RefBoilerEff)*(1-CO2Limit);
   						      			
}
  
 /******************************************************************************
 ****************************************************************************** 
 OUTPUT OF RESULTS
 ******************************************************************************
 ******************************************************************************/
  
   
  tuple UnitPur
  {
	string tech;
	float purchased;
  }
  
    tuple UnitPur2
  {
	string tech;
	float purchased;
  }

    tuple Dem
  {
	int Period;
	int enduse;
	float dem;
  }
  
    tuple Generate
  {
	int Period;
	string tech;
	int enduse;
	float energygen;
  }
       
    tuple Electricity
  {
	int Period;
	float elecbough;
  }
  
      tuple Sold
  {
	int Period;
	float elecsol;
  }
     tuple Dumping
  {
	int Period;
	string tech;
	float hdump;
  }
  
     tuple TESCh
  {
    int Period;
    float tesch;
 } 
 
      tuple TESDisch
  {
    int Period;
    float tesd;
 } 
 
      tuple TESCurrent
  {
    int Period;
    float tescur;
 } 
 
 
    tuple BatCh
  {
    int Period;
    float bch;
 } 
  
    tuple BatDisch
  {
    int Period;
    float bdicch;
 } 
 
     tuple BatCurrent
  {
    int Period;
    float cstored;
 } 
  
  
  {UnitPur} discP = {<i,Unit[i]>  |i in Tech};
  
  {UnitPur2} contP = {<i,Cap[i]>  |i in Tech};
       
  {Generate} engen = {<p,i,u,Gen[p][i][u]>  |p in Period, i in Tech, u in EndUse};
      
  {Electricity} elec = {<p,ElecPur[p]>  |p in Period};
  
  {Sold} elesell = {<p,ElecSell[p]>  |p in Period};
       
  {TESCh} teschar = {<p,Charge_TES[p]>  |p in Period};
  
  {TESDisch} tesdisch = {<p, Discharge_TES[p]>  |p in Period};
  
  {TESCurrent} tesstore = {<p,Stored_TES[p]>  |p in Period};
  
  {BatCh} batchar = {<p,Charge_Bat[p]>  |p in Period};
  
  {BatDisch} batdisch = {<p, Discharge_Bat[p]>  |p in Period};
  
  {BatCurrent} batstore = {<p,Stored_Bat[p]>  |p in Period};
  
  {Dumping} dumph = {<p,i,dump[p][i]>  |p in Period, i in Tech};
  
  {Dem} dems = {<p,u, Demand[p][u]> |p in Period, u in EndUse};
  